package main

import (
	"flag"
	"fmt"
	"path/filepath"

	pb "github.com/btcsuite/btcwallet/rpc/walletrpc"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"github.com/btcsuite/btcutil"
)

var testnet = flag.Bool("testnet", false, "operate in Bitcoin test network")
var server = flag.String("server", "localhost", "btcwallet server hostname")
var addname = flag.String("addname", "", "add account with name")
var secret = flag.String("secret", "", "private wallet password")
var certificateFile = filepath.Join(btcutil.AppDataDir("btcwallet", false), "rpc.cert")
var certpath = flag.String("cert", certificateFile, "rpc.cert path")
var acctid = flag.Int("acct", 0, "wallet account number")

func NewRcvAddress(num int) (string, error) {
	flag.Parse()
	creds, err := credentials.NewClientTLSFromFile(*certpath, *server)
	if err != nil {
		return "", err
	}
	rpcsvc := fmt.Sprintf("%s:8332", *server)
	if *testnet {
		rpcsvc = fmt.Sprintf("%s:18332", *server)
	}
	conn, err := grpc.Dial(rpcsvc, grpc.WithTransportCredentials(creds))
	if err != nil {
		return "", err
	}
	defer conn.Close()
	c := pb.NewWalletServiceClient(conn)

	addr, err := getNextAddress(c, uint32(num))
	if err != nil {
		return "", err
	}
	return addr, nil
}

func TestClient() {
	flag.Parse()
	creds, err := credentials.NewClientTLSFromFile(certificateFile, "localhost")
	if err != nil {
		fmt.Println(err)
		return
	}
	rpcsvc := "localhost:8332"
	if *testnet {
		rpcsvc = "localhost:18332"
	}
	conn, err := grpc.Dial(rpcsvc, grpc.WithTransportCredentials(creds))
	if err != nil {
		fmt.Println(err)
		return
	}
	defer conn.Close()
	c := pb.NewWalletServiceClient(conn)

	//testBalance(c)
	addName(c)
	testAccounts(c)
}

func addName(c pb.WalletServiceClient) {
	if *addname == "" {
		return
	}
	nameRequest := &pb.NextAccountRequest{
		Passphrase:  []byte(*secret),
		AccountName: *addname,
	}
	nameResponse, err := c.NextAccount(context.Background(), nameRequest)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Named acct: ", nameResponse.GetAccountNumber())
}

func getNextAddress(c pb.WalletServiceClient, acctnum uint32) (string, error) {
	nextAddressRequest := &pb.NextAddressRequest{
		Account: acctnum,
		Kind:    pb.NextAddressRequest_BIP0044_EXTERNAL,
	}

	nextAddressResponse, err := c.NextAddress(context.Background(), nextAddressRequest)
	if err != nil {
		return "", err
	}
	return nextAddressResponse.Address, nil
}

func testAddress(c pb.WalletServiceClient, acctnum uint32) {
	nextAddressRequest := &pb.NextAddressRequest{
		Account: acctnum,
		Kind:    pb.NextAddressRequest_BIP0044_EXTERNAL,
	}

	nextAddressResponse, err := c.NextAddress(context.Background(), nextAddressRequest)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Nxt addr: ", nextAddressResponse.Address)
}

func testAccounts(c pb.WalletServiceClient) {
	accountsRequest := &pb.AccountsRequest{}
	accountsResponse, err := c.Accounts(context.Background(), accountsRequest)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, a := range accountsResponse.GetAccounts() {
		num := a.GetAccountNumber()
		fmt.Printf("acct# %d (%v): %v \n",
			num,
			a.GetAccountName(),
			btcutil.Amount(a.GetTotalBalance()))
		testAddress(c, num)
	}
}

func testBalance(c pb.WalletServiceClient) {
	balanceRequest := &pb.BalanceRequest{
		AccountNumber:         0,
		RequiredConfirmations: 1,
	}
	balanceResponse, err := c.Balance(context.Background(), balanceRequest)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Spendable balance: ", btcutil.Amount(balanceResponse.Spendable))
}
