# android-btcrcv
This is a mobile app for BTC receive addresses. It's
 a quick way for me to request the next
 address for my wallet that will receive
 transaction output.


## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/btcrcv/)
3. From the cloned directory, build and run


```console
$ docker build -t btcrcv mobile
$ docker run -it --rm -v $(pwd):/go/app/btcrcv btcrcv
# cp app.apk /go/app/btcrcv/btcrcv.apk
# exit
$ adb install btcrcv.apk
```

## Requirements
This app requires the btcwallet experimental RPC service.

## Credits
Gomobile is by 
 [The golang developers](https://github.com/golang/mobile)
 (LICENSE)

