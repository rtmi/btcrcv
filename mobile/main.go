// +build darwin linux windows

//
//TODO: Without full UX, we'll hard-code service address/port and rpc user/pass
//      for local/dev use only
package main

import (
	"log"

	"golang.org/x/mobile/app"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/exp/app/debug"
	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/gl"

	"github.com/patterns/res"
)

var (
	images      *glutil.Images
	fps         *debug.FPS
	program     gl.Program
	rcvAddress  string
	requestMade bool
)

func main() {
	err := res.Initialize()
	if err != nil {
		log.Println("Check fonts exist in assets folder")
		panic(err)
	}

	app.Main(func(a app.App) {
		var glctx gl.Context
		var sz size.Event

		for e := range a.Events() {
			switch e := a.Filter(e).(type) {
			case lifecycle.Event:
				switch e.Crosses(lifecycle.StageVisible) {
				case lifecycle.CrossOn:
					glctx, _ = e.DrawContext.(gl.Context)
					onStart(glctx)
					a.Send(paint.Event{})
				case lifecycle.CrossOff:
					onStop(glctx)
					glctx = nil
				}
			case size.Event:
				sz = e
				res.Dimensions(sz.WidthPx, sz.HeightPx, sz.PixelsPerPt)
			case paint.Event:
				if glctx == nil || e.External {
					// skip system paint events
					continue
				}

				onPaint(glctx, sz)
				a.Publish()
				// Drive animation by preparing next frame
				a.Send(paint.Event{})
			case touch.Event:
				var down bool
				switch e.Type {
				case touch.TypeBegin:
					down = true
				case touch.TypeEnd:
					down = false
				}
				res.Touch(e.X, e.Y, down)
			}
		}
	})
}

func onStart(g gl.Context) {
	var err error
	program, err = glutil.CreateProgram(g, vertexShader, fragmentShader)
	if err != nil {
		log.Println("Check OpenGL prerequisites are installed")
		panic(err)
	}

	// Seek position/color/offset fields living inside the shaders
	res.BtnPos = g.GetAttribLocation(program, "position")
	res.BtnClr = g.GetUniformLocation(program, "color")

	images = glutil.NewImages(g)
	fps = debug.NewFPS(images)
	requestMade = false
}

func onStop(g gl.Context) {
	g.DeleteProgram(program)
	res.Release(g)
	fps.Release()
	images.Release()
}

func onPaint(g gl.Context, sz size.Event) {
	var err error
	// choose bkgrnd color
	g.ClearColor(0, 0, 0, 1)
	g.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	g.UseProgram(program)

	res.Prepare()
	if requestMade {
		dx := float32((res.UI.WidthPx - res.Btnwpx) / 2)
		dy := float32(res.UI.HeightPx - res.Btnhpx)
		if res.Button(g, "dismissBtn", dx, dy) {
			requestMade = false
		}
		res.Label(sz, 3, rcvAddress, images)
	} else {
		x := float32((res.UI.WidthPx - res.Btnwpx) / 2)
		y := float32(res.Headerht - res.Btnhpx)
		if res.Button(g, "generateBtn", x, y) {
			rcvAddress, err = NewRcvAddress(*acctid)
			if err != nil {
				log.Println("Check btcwallet and btcd RPC services support Next-Address: %v", err)
			} else {
				requestMade = true
			}
		}
	}

	res.Finish()

	fps.Draw(sz)
}

const vertexShader = `
	#version 100
	attribute vec4 position;
	void main() {
		gl_Position = position;
}`

const fragmentShader = `
	#version 100
	precision mediump float;
	uniform vec4 color;
	void main() {
		gl_FragColor = color;
}`
